﻿namespace FolderTrackerService
{
    public interface ILogger
    {
        void Log(string s);
    }
}