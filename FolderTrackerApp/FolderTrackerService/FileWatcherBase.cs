﻿using System;

namespace FolderTrackerService
{
    public abstract class FileWatcherBase : IDisposable
    {
        public event EventHandler<FileCreatedEventArgs> FileCreated;
        internal void OnFileCreated(FileCreatedEventArgs e)
        {
            FileCreated?.Invoke(this, e);
        }
        public abstract void Dispose();

    }
}