﻿namespace FolderTrackerService.FileHandlers
{
    public interface ITrackingFileHandler
    {
        string Name { get; }
        string Process(string content);
    }
}
