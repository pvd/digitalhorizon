﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;

namespace FolderTrackerService.FileHandlers
{
    public class PunctuationMarksCounter: ITrackingFileHandler
    {
        public string Name => "Punctuation Marks";

        public string Process(string content)
        {
            return new RegexCounter(@"[.,?\/#!$%\^&\*;:{}=\-_`~()]").Count(content).ToString();
        }
    }
}
