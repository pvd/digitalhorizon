﻿using System.IO;

namespace FolderTrackerService
{
    public abstract class TrackingFileBase
    {
        public string FilePath { get; set; }

        public TrackingFileType Type
        {
            get
            {
                switch (Extension?.ToLower())
                {
                    case ".css":
                        return TrackingFileType.Css;
                    case ".html":
                    case ".htm":
                        return TrackingFileType.Html;
                    case ".txt":
                    case ".xml":
                    case ".js":
                        return TrackingFileType.Other;
                }
                return TrackingFileType.Unknown;
            }

        }

        public string Name => Path.GetFileName(FilePath);
        public string Extension => Path.GetExtension(FilePath);

        public abstract string GetContent();

    }
}