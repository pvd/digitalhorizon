﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderTrackerService
{
    public class Logger : ILogger
    {
        private readonly string _logPath;

        public Logger(string logPath)
        {
            _logPath = logPath;
        }

        public void Log(string s)
        {
            File.AppendAllText(_logPath, s + Environment.NewLine);

        }

    }
}
