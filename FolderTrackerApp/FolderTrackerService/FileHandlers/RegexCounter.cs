﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;

namespace FolderTrackerService.FileHandlers
{
    public class RegexCounter
    {
        private readonly string _pattern;

        public RegexCounter(string pattern)
        {
            _pattern = pattern;
        }

        public int Count(string content)
        {
            return Regex.Matches(content, _pattern, RegexOptions.IgnoreCase).Count;

        }
    }
}
