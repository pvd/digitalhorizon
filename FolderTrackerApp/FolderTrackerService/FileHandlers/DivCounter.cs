﻿
namespace FolderTrackerService.FileHandlers
{
    public class DivCounter: ITrackingFileHandler
    {
        public string Name => "Div";

        public string Process(string content)
        {
            return new RegexCounter(@"<div").Count(content).ToString();
        }
    }
}
