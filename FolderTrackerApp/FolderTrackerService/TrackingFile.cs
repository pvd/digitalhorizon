﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderTrackerService
{
    public class TrackingFile : TrackingFileBase
    {
        public override string GetContent()
        {
            return File.ReadAllText(FilePath);
        }
    }
}
