﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderTrackerService
{
    
    public class FileWatcher: FileWatcherBase
    {
        private readonly FileSystemWatcher _fileSystemWatcher;
        public FileWatcher(string trackingfolderPath)
        {
            if (string.IsNullOrEmpty(trackingfolderPath))
            {
                throw new ArgumentNullException(nameof(trackingfolderPath));
            }
            if (!Directory.Exists(trackingfolderPath))
            {
                throw new Exception($"Folder {trackingfolderPath} not exists");
            }
            
            _fileSystemWatcher = new FileSystemWatcher(trackingfolderPath);
            _fileSystemWatcher.Created += (o, args) => OnFileCreated(new FileCreatedEventArgs() {TrackingFile = new TrackingFile { FilePath = args.FullPath }});
            _fileSystemWatcher.EnableRaisingEvents = true;

        }

        public override void Dispose()
        {
            _fileSystemWatcher.EnableRaisingEvents = false;
            _fileSystemWatcher.Dispose();
        }
    }

    public class FileCreatedEventArgs : EventArgs
    {
       public TrackingFileBase TrackingFile { get; set; }
    }

}
