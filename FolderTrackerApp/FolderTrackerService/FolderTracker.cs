﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FolderTrackerService.FileHandlers;

namespace FolderTrackerService
{
    public class FolderTracker : IDisposable
    {
        private readonly FileWatcherBase _fileWatcher;
      
        public FolderTracker(TrackingFileProcessors fileProcessors, FileWatcherBase fileWatcher)
        {
            if (fileProcessors == null )
            {
                throw new ArgumentException(nameof(fileProcessors));
            }
            
            _fileWatcher = fileWatcher ?? throw new ArgumentException(nameof(fileWatcher));
            _fileWatcher.FileCreated += (o, args) => fileProcessors.ProcessAll(args.TrackingFile); 
        }
        
        public void Dispose()
        { 
            _fileWatcher.Dispose();
        }
    }

}



