﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FolderTrackerService;
using FolderTrackerService.FileHandlers;

namespace FolderTrackerApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var fileProcessors = new TrackingFileProcessors(new Logger(@"c:\temp2\FolderTrackerApp1.log"));
            fileProcessors.AddHandler(TrackingFileType.Html, new DivCounter());
            fileProcessors.AddHandler(TrackingFileType.Css, new BracketsCounter());
            fileProcessors.AddHandler(TrackingFileType.Other, new PunctuationMarksCounter());
            var fileWatcher = new FileWatcher(@"c:\temp\");
            using (new FolderTracker(fileProcessors, fileWatcher))
            {
                Console.WriteLine("Press 'q' to quit the sample.");
                while (Console.Read() != 'q')
                {
                }
            }
        }
    }
}
