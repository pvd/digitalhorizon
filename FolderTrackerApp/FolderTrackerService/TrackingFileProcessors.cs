﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using FolderTrackerService.FileHandlers;

namespace FolderTrackerService
{
    public class TrackingFileProcessors
    {
        private readonly ILogger _logger;
        private Dictionary<TrackingFileType, List<ITrackingFileHandler>> _handlersDictionary;
        public void AddHandler(TrackingFileType fileType, ITrackingFileHandler handler)
        {
            if (_handlersDictionary == null)
            {
                _handlersDictionary = new Dictionary<TrackingFileType, List<ITrackingFileHandler>>();
            }

            if (!_handlersDictionary.ContainsKey(fileType))
            {
                _handlersDictionary[fileType] = new List<ITrackingFileHandler>();
            }

            if (_handlersDictionary[fileType].All(x => x.GetType() != handler.GetType()))
            {
                _handlersDictionary[fileType].Add(handler);
            }
            else
            {
                throw new Exception($"This handler {handler.GetType()} allredy exists for file type {fileType}");
            }
        }

        public void ProcessAll(TrackingFileBase file)
        {
            if (_handlersDictionary.ContainsKey(file.Type))
            {
                foreach (var handler in _handlersDictionary[file.Type])
                {
                    try
                    {
                        var result = handler.Process(file.GetContent());
                        if (!string.IsNullOrWhiteSpace(result))
                        {
                            _logger.Log($"{file.Name}-{handler.Name}-{result}");
                        }
                    }
                    catch (Exception ex)
                    {
                        _logger.Log($"{file.Name}-{handler.Name}-error: { ex.Message}");
                    }
                }
            }
        }

        public TrackingFileProcessors(ILogger logger)
        {
            _logger = logger;
        }

    }
}
