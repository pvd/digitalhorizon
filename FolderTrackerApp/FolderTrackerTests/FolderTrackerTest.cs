﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FolderTrackerService;
using FolderTrackerService.FileHandlers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FolderTrackerTests
{
    [TestClass]
    public class FolderTrackerTest
    {
        
        [TestMethod]
        public void FolderTrackerCompleteTest()
        {
            var logger = new TestLogger();
            var fileProcessors = new TrackingFileProcessors(logger);
            var divCounter = new DivCounter();
            fileProcessors.AddHandler(TrackingFileType.Html, divCounter);
            Assert.ThrowsException<Exception>(() =>
                fileProcessors.AddHandler(TrackingFileType.Html, divCounter));

            var bracketsCounter = new BracketsCounter();
            fileProcessors.AddHandler(TrackingFileType.Css, bracketsCounter);

            var punctuationMarksCounter = new PunctuationMarksCounter();
            fileProcessors.AddHandler(TrackingFileType.Other, punctuationMarksCounter);

            var fileWatcher = new TestFileWatcher();
           
            using (new FolderTracker(fileProcessors, fileWatcher))
            {
                fileWatcher.OnFileCreated(new FileCreatedEventArgs()
                {
                    TrackingFile = new TestTrackingFile("Я люблю яблоки, а ты?") { FilePath = @"c:\test folder\test1.txt" }
                });

                Assert.AreEqual(1, logger.LogLines.Count);
                Assert.AreEqual($"test1.txt-{punctuationMarksCounter.Name}-2", logger.LogLines[0]);

                fileWatcher.OnFileCreated(new FileCreatedEventArgs()
                {
                    TrackingFile = new TestTrackingFile("<div></div><div></div>") { FilePath = @"c:\test folder\test2.html" }
                });

                Assert.AreEqual(2, logger.LogLines.Count);
                Assert.AreEqual($"test2.html-{ divCounter.Name}-2", logger.LogLines[1]);

                fileWatcher.OnFileCreated(new FileCreatedEventArgs()
                {
                    TrackingFile = new TestTrackingFile("{{ }") { FilePath = @"c:\test folder\test3.css" }
                });
                Assert.AreEqual(3, logger.LogLines.Count);
                Assert.AreEqual($"test3.css-{bracketsCounter.Name}-open count: 2; closed count: 1", logger.LogLines[2]);
            }
        }


        [TestMethod]
        public void PunctuationMarksCounterTest()
        {
            var counter = new PunctuationMarksCounter();
            var res = counter.Process("Я люблю яблоки, а ты?");
            Assert.AreEqual("2", res);

        }

        [TestMethod]
        public void DivCounterTest()
        {
            var counter = new DivCounter();
            var res = counter.Process("<div></div><div></div>");
            Assert.AreEqual("2", res);

        }
        [TestMethod]
        public void BracketsCounterTest()
        {
            var counter = new BracketsCounter();
            var res = counter.Process("{{ }");
            Assert.AreEqual("open count: 2; closed count: 1", res);

        }

        public class TestLogger : ILogger
        {
            public List<string> LogLines { get; set; }
            public void Log(string message)
            {
                LogLines.Add(message);
            }
            public TestLogger()
            {
                LogLines = new List<string>();
            }
        }

        public class TestTrackingFile : TrackingFileBase
        {
            private readonly string _content;
            public TestTrackingFile(string content)
            {
                _content = content;
            }
            public override string GetContent()
            {
                return _content;
            }
        }
        
        public class TestFileWatcher: FileWatcherBase
        {
            public override void Dispose()
            {
                
            }
        }

    }
}
