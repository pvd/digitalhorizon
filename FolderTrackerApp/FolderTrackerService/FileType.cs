﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderTrackerService
{
    public enum TrackingFileType
    {
        Unknown = 0,
        Css=1,
        Html = 2,
        Other = 3
    }
}
