﻿using System;
using System.Collections.Generic;

namespace FolderTrackerService.FileHandlers
{
    public class BracketsCounter: ITrackingFileHandler
    {

        public string Name => "brackets";

        public string Process(string content)
        {
            return $"open count: {new RegexCounter(@"{").Count(content)}; closed count: {new RegexCounter(@"}").Count(content)}";
        }
    }
}
